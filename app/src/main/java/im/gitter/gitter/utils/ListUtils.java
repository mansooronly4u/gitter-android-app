package im.gitter.gitter.utils;

import java.util.ArrayList;
import java.util.List;

public class ListUtils<From, To> {

    public List<To> map(List<From> from, Converter<From, To> converter) {
        List<To> to = new ArrayList<>();
        for (int i = 0; i < from.size(); i++) {
            to.add(i, converter.convert(from.get(i), i, from));
        }

        return to;
    }

    public interface Converter<From, To> {
        public To convert(From currentValue, int index, List<From> from);
    }
}
