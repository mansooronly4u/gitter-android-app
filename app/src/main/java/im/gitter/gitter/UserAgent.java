package im.gitter.gitter;

import android.content.Context;
import android.content.pm.PackageManager;

public class UserAgent {

    public static String get(Context context) {
        // Dalvik/1.6.0 (Linux; U; Android 4.4.4; GT-I9300 Build/KTU84P)
        String systemUserAgent = System.getProperty("http.agent");
        return addGitterDetails(context, systemUserAgent);
    }

    public static String addGitterDetails(Context context, String existingUserAgent) {
        String userAgent = existingUserAgent;

        if (!userAgent.contains("Gitter")) {
            String version;
            try {
                version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                version = "0.0.0";
            }

            userAgent = userAgent + " Gitter/" + version;

            if (!userAgent.contains("Mobile")) {
                userAgent = userAgent + " Mobile";
            }
        }

        return userAgent;
    }
}
